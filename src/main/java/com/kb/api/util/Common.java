package com.kb.api.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.springframework.stereotype.Component;
import org.jsoup.Jsoup;

/**
 * @version : java1.8
 * @author : ohs
 * @date : 2018. 8. 8.
 * @class :
 * @message : API 서버 공통 소스
 * @constructors :
 * @method :
 */
@Component
public class Common {
	
	String fileSavePath = "F:\\ohs\\PROJECT\\PROJECT\\goodbaby\\src\\main\\webapp\\resources\\images\\";
	/**
	 * 파일다운로드
	 * @param URL
	 * @param filename
	 */
    public void download(final String URL, final String filename, int timeOut) {
        Thread thread = new Thread(new Runnable() {
            public synchronized void run() {
//                System.out.println( "[파일업로드 시작] "+ Thread.currentThread().getName());
//                System.out.println( "[파일업로드 URL] "+ URL);
                
                Connection conn = Jsoup.connect(URL);
                conn.maxBodySize(0);
                conn.ignoreContentType(true);
                conn.timeout(timeOut);
                
                try {
                    Response response = conn.execute();
                    
                    File saveFile = new File(fileSavePath + filename);
                    FileOutputStream out = new FileOutputStream(saveFile);

                    out.write( response.bodyAsBytes());
                    out.close();
                    
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
//                    System.out.println( "[파일업로드 종료]"+ Thread.currentThread().getName());
                }
            }
        }, "FILE_UPLOAD");
        thread.start();
    }
	
}
