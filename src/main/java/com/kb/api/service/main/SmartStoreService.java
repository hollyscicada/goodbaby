package com.kb.api.service.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.parser.ParseException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.kb.api.util.Common;

@Service
public class SmartStoreService {

	
	String httpSmartStoreUrl = "https://smartstore.naver.com";
	@Inject Common common;
	int timeOut = 1000*60*60;
	
	public Map<String, Object> main(HttpServletRequest request) throws IOException, ParseException, InterruptedException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", true);
		String storeId = request.getParameter("storeId");
		String categoryId = request.getParameter("categoryId");
		String pageSize = "20";
		
		if(StringUtils.isEmpty(storeId)) {
			map.put("success", false);
			map.put("errorMsg", "storeId를 입력해주세요.");
			return map;
		}
		if(StringUtils.isEmpty(categoryId)) {
			map.put("success", false);
			map.put("errorMsg", "categoryId를 입력해주세요.");
			return map;
		}
		if(!StringUtils.isEmpty(request.getParameter("pageSize"))) {
			pageSize = request.getParameter("pageSize");
		}
		
		if(!StringUtils.isEmpty(request.getParameter("pageSize"))) {
			pageSize = request.getParameter("pageSize");
			
			if(Integer.parseInt(pageSize) < 20) {
				map.put("success", false);
				map.put("errorMsg", "pageSize는 최소 20이상부터 가능합니다.");
				return map;
			}
		}
		
		String url = httpSmartStoreUrl+"/"+storeId+"/category/"+categoryId+"?st=POPULAR&free=false&dt=IMAGE&page=1&size="+pageSize;
		String referal = httpSmartStoreUrl+"/"+storeId+"/category/"+categoryId+"?cp=1";
		
		
         
		Document doc = Jsoup.connect(url)
		        .header("origin", referal) // same-origin-polycy 로 인한 설정
		        .header("referer", referal) // same-origin-polycy 로 인한 설정
		        .ignoreContentType(true) // json 받아오려면 타입무시를 해야하는듯?
		        .timeout(timeOut)
		        .get();
         
         
         Elements items = doc.select(".-qHwcFXhj0");
         List<Map<String, Object>> rstList = new ArrayList<>();
         int i = 0;
         for( Element item : items ) {
        	 if(i < 5) {
        		 i++;
        		 
        		 Map<String, Object> rstMap = new HashMap<String, Object>();
        		 
        		 Elements idTag = item.select("._3BkKgDHq3l"); //id  
            	 Elements nameTag = item.select(".QNNliuiAk3"); //상품명
            	 Elements priceTag = item.select("._23DThs7PLJ strong .nIAdxeTzhx"); //금액
            	 Elements bepriceTag = item.select("._45HSXeff1y .nIAdxeTzhx"); //할인전 금액
            	 Elements discountTag = item.select(".pT4bw14aV2"); //할인율
            	 
            	 
            	 String href = idTag.attr("href");
            	 String id = href.substring(href.lastIndexOf("/")+1, href.length());
            	 
            	 String name = nameTag.text();
            	 String price = priceTag.text();
            	 String beprice = bepriceTag.text();
            	 String discount = discountTag.text();
            	 
            	 
            	 /**
     			 * 기본정보 가져오기
     			 */
     			/*
            	 System.out.println("=============================================================");
            	 System.out.println("상세페이지 URL : " + href);
            	 System.out.println("ID : " + id);
            	 System.out.println("상품명 : " + name);
            	 System.out.println("금액 : " + price);
            	 System.out.println("할인전 금액 : " + beprice);
            	 System.out.println("할인율 : " + discount);
            	 System.out.println("=============================================================");
            	 System.out.println("");
            	 System.out.println("");
            	 System.out.println("");
            	 System.out.println("");
            	 */
            	 
            	 
            	 /**
     			 * 이미지 가져오기(상세페이지에 가서 가져온다)
     			 */
            	List<Map<String, Object>> imgList = new ArrayList<>();
     			if(!StringUtils.isEmpty(href) && !StringUtils.isEmpty(id)) {
     				imgList = this.getImg(href, id);
     			}
     			
     			
     			/**
     			 * 상세페이지 접근
     			 */
     			if(!StringUtils.isEmpty(href) && !StringUtils.isEmpty(id)) {
     				
     				
     				Connection conn = Jsoup.connect(httpSmartStoreUrl + href).timeout(timeOut);
     		        Document html = conn.get(); // conn.post();
     		        
     		        String productNo = html.toString().substring(html.toString().indexOf("\"productNo\":\""), html.toString().indexOf("\",\"salePrice"));
     		        productNo = productNo.replaceAll("\"productNo\":\"", "");
     				
     				if(!StringUtils.isEmpty(productNo)) {
        				String renderContent = this.sub(id, productNo);
        				rstMap.put("renderContent", renderContent);
     				}
     			}
        		 
     			
     			/**
    			 * 리턴 데이터 셋팅
    			 */
     			rstMap.put("name", name);
    			rstMap.put("imgs", imgList);
    			rstList.add(rstMap);
        	 }
        	 
         }
         
         map.put("data", rstList);
 		return map;
	}
	
	/**
	 * 이미지 가져오기(상세페이지에 가서 가져온다)
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public List<Map<String, Object>> getImg(String uri, String id) throws IOException, InterruptedException{
		List<Map<String, Object>> imgList = new ArrayList<>();
		
		
		Connection conn = Jsoup.connect(httpSmartStoreUrl + uri).timeout(timeOut);
        Document html = conn.get(); // conn.post();
        
        
        Elements imgItems = html.select("._1F-riHNwNb img");
        for( Element imgItem : imgItems ) {
        	Map<String, Object> imgMap = new HashMap<String, Object>();
        	 
        	
        	String fullPath = imgItem.attr("src");
        	fullPath = fullPath.substring(0, fullPath.lastIndexOf("?")+1)+"type=m510";
        	String fileName = id+"_"+fullPath.substring(fullPath.lastIndexOf("/")+1, fullPath.lastIndexOf("?"));
        	common.download(fullPath, fileName, timeOut);
			
			imgMap.put("url", "/resources/images/"+fileName);
			imgList.add(imgMap);
        }
        return imgList;
	}
	
	/**
	 * 상세페이지 
	 * @throws InterruptedException 
	 */
	
	public String sub(String id, String productNo) throws IOException, ParseException, InterruptedException {
		
		String fb_url = "https://shopping.naver.com/v1/products/"+id+"/contents/"+productNo+"/PC";
		URL url = new URL(fb_url);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setDoOutput(true);
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		con.setConnectTimeout(timeOut);
		con.setReadTimeout(timeOut);

		int responseCode = con.getResponseCode();
		BufferedReader br;
		if (responseCode == 200) {
			br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
		} else {
			br = new BufferedReader(new InputStreamReader(con.getErrorStream(), "utf-8"));
		}
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = br.readLine()) != null) {
			response.append(inputLine);
		}
		br.close();
		String json = response.toString();
		
		/**
		 * HTML 데이터 파싱
		 */
		String renderContent = json.substring(json.indexOf("\"renderContent\":\""), json.length());
		renderContent = renderContent.replaceAll("\"renderContent\":\"", "");
		renderContent = renderContent.substring(0, renderContent.lastIndexOf("}")-1);
		
		
		/**
		 * HTML에서 img src 추출후 이미지 로컬에 업로드
		 */
		String subRenderContent = renderContent.replaceAll("\\\\", "");
		Pattern pattern = Pattern.compile("<img[^>]*src=[\"']?([^>\"']+)[\"']?[^>]*>"); //img 태그 src 추출 정규표현식
        Matcher matcher = pattern.matcher(subRenderContent);
         
        while(matcher.find()){
        	String fullPath = matcher.group(1);
        	String fileName = id+"_"+fullPath.substring(fullPath.lastIndexOf("/")+1, fullPath.length());
			common.download(fullPath, fileName, timeOut);
			String currentUrl = "/resources/images/"+fileName;
			renderContent = renderContent.replaceAll(fullPath, currentUrl);
        }
		return renderContent;
	}
	
	
	
	
	
}
