package com.kb.api.controller.main;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.main.NaverService;
import com.kb.api.service.main.NaverSortService;
import com.kb.api.service.main.SmartStoreService;

@RestController
public class MainRestController {
	
	/**
	 * 네이버쇼핑(스토어기준)
	 */
	@Inject NaverService naverService;
	
	/**
	 * 네이버쇼핑(전체)정렬기준
	 */
	@Inject NaverSortService naverSortService;
	
	/**
	 * 스마트스토어
	 */
	@Inject SmartStoreService smartStoreService;
	
	
	
	@GetMapping("/api/v1/naver")  
	public ResponseEntity<Map<String, Object>> main(HttpServletRequest request) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = naverService.main(request);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
//		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	 
	
	@GetMapping("/api/v1/smartstore")  
	public ResponseEntity<Map<String, Object>> smartstore(HttpServletRequest request) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = smartStoreService.main(request);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
//		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	@GetMapping("/api/v1/naver/sort")  
	public ResponseEntity<Map<String, Object>> mainSort(HttpServletRequest request) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = naverSortService.main(request);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
//		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
}
