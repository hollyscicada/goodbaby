package com.kb.api.controller.main;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kb.api.service.main.NaverService;

@Controller
public class MainController {  
	
	@Inject NaverService mainService;
	@RequestMapping(value = "/naver")
	public String index(Model model, HttpServletResponse response, HttpServletRequest request, HttpSession session) throws Exception {
		return "naver";
	}
	
	@RequestMapping(value = "/smartstore")
	public String smartstore(Model model, HttpServletResponse response, HttpServletRequest request, HttpSession session) throws Exception {
		return "smartstore";
	}
	
	@RequestMapping(value = "/naver/sort")
	public String test(Model model, HttpServletResponse response, HttpServletRequest request, HttpSession session) throws Exception {
		return "naverSort";
	}
}
